from driver.sdcard import SDCard
import machine
import json
import uos
import sys
import gc


def get_configuration():
    spi_sd = machine.SPI(1, sck=machine.Pin(10), miso=machine.Pin(12), mosi=machine.Pin(11))
    sd = SDCard(spi_sd, machine.Pin(22, machine.Pin.OUT))

    vfs = uos.VfsFat(sd)
    uos.mount(vfs, "/sd")

    with open("/sd/configuration.json", "r") as file:
        data = file.read()
        configuration = json.loads(data)

    uos.umount("/sd")
    del spi_sd
    del sd
    del vfs
    del sys.modules["driver.sdcard"]
    del sys.modules["driver"]
    gc.collect()

    return configuration
