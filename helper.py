from display import Display35
import gc
import colors


def print_header(display: Display35, text: str = "", line: int = 1):
    y = 13
    if line == 2:
        y = 29
    header = display.get_area(x=21, y=y, width=422, height=8)
    header.fill(colors.PRIMARY)
    header.text(text, 0, 0, colors.ON_PRIMARY)
    header.show()
    del header
    gc.collect()


def print_footer_all(display: Display35, text: str):
    header = display.get_area(x=0, y=304, width=480, height=16)
    header.fill(colors.PRIMARY)
    header.text(text, 4, 5, colors.ON_PRIMARY)
    header.show()
    del header
    gc.collect()


def print_footer_left(display: Display35, text: str = ""):
    header = display.get_area(x=0, y=304, width=240, height=16)
    header.fill(colors.PRIMARY)
    header.text(text, 4, 5, colors.ON_PRIMARY)
    header.show()
    del header
    gc.collect()


def print_footer_right(display: Display35, text: str = ""):
    header = display.get_area(x=240, y=304, width=240, height=16)
    header.fill(colors.PRIMARY)
    x = 240 - 4 - 8 * len(text.encode('utf-8'))
    if x < 0:
        x = 0
    header.text(text, x, 5, colors.ON_PRIMARY)
    header.show()
    del header
    gc.collect()


def clear_footer(display: Display35):
    header = display.get_area(x=0, y=304, width=display.width, height=16)
    header.fill(colors.PRIMARY)
    header.show()
    del header
    gc.collect()


def fill_area(display: Display35, x: int, y: int, width: int, height: int):
    fill = display.get_area(x=x, y=y, width=width, height=height)
    fill.fill(colors.SURFACE)
    fill.show()
    del fill
    gc.collect()


def clear_header(display: Display35):
    header = display.get_area(x=0, y=0, width=display.width, height=50)
    header.fill(colors.PRIMARY)
    header.show()
    del header
    gc.collect()


def clear_body(display: Display35):
    fill_area(display, 0, 50, display.width, 50)
    fill_area(display, 0, 100, display.width, 50)
    fill_area(display, 0, 150, display.width, 50)
    fill_area(display, 0, 200, display.width, 50)
    fill_area(display, 0, 250, display.width, 54)


def free():
    f = gc.mem_free()
    a = gc.mem_alloc()
    t = f + a
    p = '{0:.2f}%'.format(f / t * 100)
    return 'Total:{0} Free:{1} ({2})'.format(t, f, p)
