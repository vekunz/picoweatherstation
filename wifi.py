from display import Display35
import helper
import network
import time

wlan: network.WLAN


def connect(display: Display35, config, status):
    global wlan

    try:
        wlan = network.WLAN(network.STA_IF)
        wlan.active(True)
        wlan.config(pm=0xa11140)

        status("Scan WiFi")
        networks = wlan.scan()
        network_names = []
        for netw in networks:
            network_names.append(netw[0].decode("utf-8"))
        print(network_names)

        wifi_config = {}

        for config_network in config["locations"]:
            if config_network["ssid"] in network_names:
                wifi_config = config_network

        wlan.connect(wifi_config["ssid"], wifi_config["password"])

        # Wait for connect or fail
        max_wait = 10
        while max_wait > 0:
            if wlan.status() < 0 or wlan.status() >= 3:
                break
            max_wait -= 1
            status("Waiting for connection")
            time.sleep(1)

        # Handle connection error
        if wlan.status() != 3:
            status("Network connect failed " + str(wlan.status()))
            return False
        else:
            status = wlan.ifconfig()
            helper.print_footer_left(display, status[0])
            return True
    except Exception as e:
        helper.print_footer_left(display, str(e))
        return False


def name():
    return wlan.config('ssid')


def wifi_symbol():
    b = [0x00, 0xf8]
    c = [0xff, 0xff]
    # w:25 h:20

    symbol = [
        b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b, c, c, c, c, c, c, c, c, c, b, b, b, b, b, b, b, b,
        b, b, b, b, b, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, b, b, b, b, b,
        b, b, b, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, b, b, b,
        b, b, c, c, c, c, c, c, b, b, b, b, b, b, b, b, b, c, c, c, c, c, c, b, b,
        b, c, c, c, c, c, b, b, b, b, b, b, b, b, b, b, b, b, b, c, c, c, c, c, b,
        b, c, c, c, b, b, b, b, b, b, c, c, c, c, c, b, b, b, b, b, b, c, c, c, b,
        b, b, c, b, b, b, b, c, c, c, c, c, c, c, c, c, c, c, b, b, b, b, c, b, b,
        b, b, b, b, b, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, b, b, b, b, b,
        b, b, b, b, c, c, c, c, c, c, b, b, b, b, b, c, c, c, c, c, c, b, b, b, b,
        b, b, b, b, b, c, c, c, b, b, b, b, b, b, b, b, b, c, c, c, b, b, b, b, b,
        b, b, b, b, b, b, c, b, b, b, b, c, c, c, b, b, b, b, c, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b, b, c, c, c, c, c, c, c, b, b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b, c, c, c, c, c, c, c, c, c, b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b, b, c, c, b, b, b, c, c, b, b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b, b, b, b, c, c, c, b, b, b, b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b, b, b, b, b, c, b, b, b, b, b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b,
    ]
    return bytearray([x for xs in symbol for x in xs])
