# Weather Station

This is a simple weather station for the Raspberry Pi Pico W. It uses https://openweathermap.org to retrieve weather data.

- Microcontroller: Raspberry Pi Pico W
- Display: https://www.waveshare.com/pico-restouch-lcd-3.5.htm

The configuration is stored on an SD Card. The above-mentioned display has an MicroSD slot.

On the SD Card you need one file named `configuration.json` with the following structure:

```json
{
    "openweather_appid": "app id",
    "country": "DE",
    "locations": [
        {
            "ssid": "wifi ssid",
            "password": "wifi password",
            "lat": 0.00,
            "lon": 0.00,
            "timezone": "Europe/Berlin"
        }
    ],
    "strings": {
        "title": "Wetterstation",
        "weather": {
            "update": "Aktualisiere",
            "updated": "Aktualisiert",
            "error": "Fehler beim Abrufen der Wetterdaten",
            "now": "Jetzt",
            "wind": "Wind",
            "clouds": "Wolken",
            "no_rain_1": "Kein",
            "no_rain_2": "Regen",
            "rain": "Regen",
            "today": "Heute"
        },
        "days": {
            "monday": "Montag",
            "tuesday": "Dienstag",
            "wednesday": "Mittwoch",
            "thursday": "Donnerstag",
            "friday": "Freitag",
            "saturday": "Samstag",
            "sunday": "Sonntag"
        }
    }
}
```

You can store multiple locations. The script selects the location based on the WiFi networks available.

## Copyright notice

This project is licensed under the MIT license. However, the weather icons are property of [OpenWeather](https://openweathermap.org/weather-conditions) and therefore not covered by the MIT license, but OpenWeather grants free usage of the icons.