import colors
from display import Display35
import gc
import time
import machine
import rp2
import helper
import weather
import wifi
import clock
import location
import sys


def status(text: str):
    print(text)
    startup_log.fill_rect(0, 0, startup_log.width, 8, colors.SURFACE)
    startup_log.text(text, 0, 0, colors.ON_SURFACE)
    startup_log.show()
    startup_log.scroll(0, 12)


def load_configuration():
    import configuration
    config = configuration.get_configuration()
    del sys.modules["configuration"]
    return config


# Init system
machine.Pin("LED", machine.Pin.OUT).value(1)

print(helper.free())
# Configuration
config = load_configuration()
gc.collect()

print(config)
print(helper.free())

rp2.country(config["country"])

# Init display
screen = Display35()
screen.backlight(100)

helper.clear_header(screen)
helper.print_header(screen, "Wetterstation", line=1)
helper.clear_body(screen)
helper.clear_footer(screen)

startup_log = screen.get_area(15, 60, 400, 100)
startup_log.fill(colors.SURFACE)

city_lon = 0
city_lat = 0
timezone = ""

try:
    status("Initializing system")

    # Init WiFi
    status("Initializing WiFi")
    connected = wifi.connect(display=screen, config=config, status=status)
    if connected:
        area = screen.get_area(x=443, y=15, width=25, height=20)
        area.set_buffer(wifi.wifi_symbol())
        area.show()
        del area
        gc.collect()
    else:
        raise Exception("Failed connecting WiFi")
    status("Connected: " + wifi.name())

    for configLocation in config["locations"]:
        if configLocation["ssid"] == wifi.name():
            city_lat = configLocation["lat"]
            city_lon = configLocation["lon"]
            timezone = configLocation["timezone"]

    del sys.modules["wifi"]

    # Init clock
    status("Initializing clock")
    if clock.init_clock(timezone):
        helper.print_footer_right(screen)
        gc.collect()
    else:
        raise Exception("Failed initializing clock")

    # Init location
    status("Initializing location")
    loc = location.get_location_data(lon=city_lon, lat=city_lat, appid=config["openweather_appid"])
    helper.print_header(screen, "Wetterstation, " + loc.name, line=1)
    del loc
    del sys.modules["location"]
    gc.collect()
except Exception as e:
    status(str(e))
    status("Exit")
    raise SystemExit

status("Initializing weather")
del startup_log
gc.collect()

try:
    # Init weather
    now = clock.get_time()
    helper.print_header(screen, "{weekday}, {day:02d}.{month:02d}.{year}".format(weekday=clock.weekday_name(now.weekday), day=now.day, month=now.month, year=now.year), line=2)
    weather.show_weather(screen, lon=city_lon, lat=city_lat, appid=config["openweather_appid"], init=True)
    last_update_minute = now.minute
    gc.collect()

    # Main loop
    while True:
        now = clock.get_time()

        if last_update_minute != now.minute and now.minute % 2 == 0:
            helper.print_header(screen, "{weekday}, {day:02d}.{month:02d}.{year}".format(weekday=clock.weekday_name(now.weekday), day=now.day, month=now.month, year=now.year), line=2)
            weather.show_weather(screen, lon=city_lon, lat=city_lat, appid=config["openweather_appid"])
            last_update_minute = now.minute

        gc.collect()
        time.sleep_ms(100)
except Exception as e:
    print(e)
    helper.print_footer_all(screen, "Fatal Error: " + str(e))
