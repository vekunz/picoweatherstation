import helper
import clock
from display import Display35
import urequests
import gc
import sys
import math
import colors

COLOR_BACKGROUND = 0xFFFF
COLOR_LINES = 0x55AD
COLOR_TIME = 0x0000
COLOR_TEMP = 0x8080
COLOR_WIND = 0xCF7B
COLOR_RAIN = 0xf800
COLOR_RAIN_AMOUNT = 0x1F97


def show_weather(display: Display35, lon: float, lat: float, appid: str, init=False):
    try:
        print(helper.free())
        print("Load weather data")
        helper.print_footer_right(display, "Aktualisiere")
        gc.collect()
        print(helper.free())

        r = urequests.get("https://api.openweathermap.org/data/3.0/onecall?lat={lat}&lon={lon}&appid={appid}&exclude=minutely,alerts&units=metric&lang=de".format(lat=lat, lon=lon, appid=appid))
        if r.status_code >= 400:
            text_area = display.get_area(x=20, y=70, width=350, height=36)
            text_area.fill(colors.SURFACE)
            text_area.text("Fehler beim Abrufen der Wetterdaten", 0, 0, colors.ON_SURFACE)
            text_area.text(str(r.status_code) + " " + str(r.reason), 0, 12, colors.ON_SURFACE)
            if r.status_code < 500:
                data = r.json()
                text_area.text(data["message"], 0, 24, colors.ON_SURFACE)
                del data
            text_area.show()
            del text_area
            del r
            gc.collect()
            return
        data = r.json()
        del r
        gc.collect()
        print(helper.free())

        if init:
            helper.clear_body(display)
            gc.collect()

        print(helper.free())

        # Current Weather
        current_weather(display, data)
        gc.collect()
        print(helper.free())

        # Today Weather
        future_weather(display, data["daily"][0], x=160, y=50)
        gc.collect()
        print(helper.free())

        # Tomorrow Weather
        future_weather(display, data["daily"][1], x=160, y=101)
        gc.collect()
        print(helper.free())

        # Day after tomorrow Weather
        future_weather(display, data["daily"][2], x=320, y=50)
        gc.collect()
        print(helper.free())

        # Day after the day after tomorrow Weather
        future_weather(display, data["daily"][3], x=320, y=101)
        gc.collect()
        print(helper.free())

        del data["daily"]
        gc.collect()

        # Time and Temperature
        print("time_and_temperature")
        time_and_temperature(display, data)
        gc.collect()
        print(helper.free())

        # Weather symbols
        print("weather_symbols")
        weather_symbols(display, data)
        gc.collect()
        print(helper.free())

        # Wind
        print("wind")
        wind(display, data)
        gc.collect()
        print(helper.free())

        # Rain
        print("rain")
        rain(display, data)
        gc.collect()
        print(helper.free())

        del data
        gc.collect()
        print(helper.free())

        now = clock.get_time()
        helper.print_footer_right(display, "Aktualisiert {hour:02d}:{minute:02d}".format(hour=now.hour, minute=now.minute))
        print("Weather data loaded")
        print(helper.free())
    except Exception as e:
        helper.print_footer_right(display, str(e))
        print(e)
        print(helper.free())
        gc.collect()


def current_weather(display: Display35, data):
    current_area = display.get_area(x=0, y=50, width=160, height=102)
    current_area.fill(COLOR_BACKGROUND)
    current_area.hline(0, 101, 160, COLOR_LINES)

    # Temperature
    current_area.text("Jetzt", 5, 12, COLOR_TIME)
    current_area.text("{temp:.1f}C".format(temp=round(data["current"]["temp"], 1)).replace(".", ","), 5, 28, COLOR_TEMP)

    # Icon
    import images.weather_images
    icon = images.weather_images.get_image(data["current"]["weather"][0]["icon"])
    current_area.add_buffer(icon, 110, 0, 50)

    # Wind and Clouds
    current_area.text("Wind: {wind:.1f}km/h".format(wind=round(data["current"]["wind_speed"] * 3.6, 1)).replace(".", ","), 5, 63, COLOR_WIND)
    current_area.text("Wolken: {clouds}%".format(clouds=data["current"]["clouds"]), 5, 79, COLOR_WIND)

    # Rain
    rain_area_x = 55
    rain_area_y = 0

    rain_mm = 0
    if "rain" in data["current"]:
        rain_mm = math.ceil(data["current"]["rain"]["1h"])

    if rain_mm == 0:
        current_area.text("Kein", rain_area_x + 9, rain_area_y + 14, COLOR_RAIN)
        current_area.text("Regen", rain_area_x + 6, rain_area_y + 26, COLOR_RAIN)
    else:
        bar_height = 50 - rain_mm
        if bar_height < 0:
            bar_height = 0
        current_area.fill_rect(rain_area_x, rain_area_y + bar_height, 50, 50 - bar_height, COLOR_RAIN_AMOUNT)

        rain_mm_text = "{rain}L/m".format(rain=rain_mm)
        current_area.text("Regen", rain_area_x + 5, rain_area_y + 12, COLOR_RAIN)
        current_area.text(rain_mm_text, rain_area_x + 5 + (40 - (len(rain_mm_text) * 8)) // 2, rain_area_y + 28, COLOR_RAIN)
        del bar_height
        del rain_mm_text

    current_area.show()
    del rain_mm
    del icon
    del sys.modules["images.weather_images"]
    del sys.modules["images"]
    del current_area
    gc.collect()


def future_weather(display: Display35, data, x: int, y: int):
    tomorrow_area = display.get_area(x=x, y=y, width=160, height=51)
    tomorrow_area.fill(COLOR_BACKGROUND)
    tomorrow_area.hline(0, 50, 160, COLOR_LINES)
    tomorrow_area.vline(0, 0, 51, COLOR_LINES)

    area_1_base = 3
    area_2_base = 55
    area_3_base = 110

    date_text = ""
    now = clock.get_time()
    date = clock.get_time_timestamp(data["dt"])
    if now.day == date.day and now.month == date.month and now.year == date.year:
        date_text = "Heute"
    else:
        weekday = clock.weekday_name(date.weekday)
        if len(weekday) > 6:
            weekday = weekday[:5] + "."
        date_text = weekday

    tomorrow_area.text(date_text, area_1_base + (50 - (len(date_text) * 8)) // 2, 6, COLOR_TIME)
    tomorrow_area.text("{temp:.1f}C".format(temp=round(data["temp"]["max"], 1)).replace(".", ","), area_1_base + 5, 21, COLOR_TEMP)
    tomorrow_area.text("{temp:.1f}C".format(temp=round(data["temp"]["min"], 1)).replace(".", ","), area_1_base + 5, 35, COLOR_TEMP)

    import images.weather_images
    icon = images.weather_images.get_image(data["weather"][0]["icon"])
    tomorrow_area.add_buffer(icon, area_2_base, 0, 50)

    rain_percent = int(data["pop"] * 100)
    rain_mm = 0
    if "rain" in data:
        rain_mm = math.ceil(data["rain"])

    if rain_mm == 0 and rain_percent == 0:
        tomorrow_area.text("Kein", area_3_base + 9, 14, COLOR_RAIN)
        tomorrow_area.text("Regen", area_3_base + 5, 26, COLOR_RAIN)
    else:
        y = 50 - rain_mm
        if y < 0:
            y = 0
        tomorrow_area.fill_rect(area_3_base, y, 50, 50 - y, COLOR_RAIN_AMOUNT)

        rain_percent_text = "{percent}%".format(percent=rain_percent)
        rain_mm_text = "{rain}L/m".format(rain=rain_mm)
        tomorrow_area.text("Regen", area_3_base + 5, 6, COLOR_RAIN)
        tomorrow_area.text(rain_percent_text, area_3_base + 5 + (40 - (len(rain_percent_text) * 8)) // 2, 21, COLOR_RAIN)
        tomorrow_area.text(rain_mm_text, area_3_base + 5 + (40 - (len(rain_mm_text) * 8)) // 2, 35, COLOR_RAIN)
        del y
        del rain_percent_text
        del rain_mm_text

    tomorrow_area.show()
    del rain_percent
    del rain_mm
    del icon
    del sys.modules["images.weather_images"]
    del sys.modules["images"]
    del tomorrow_area
    gc.collect()


def time_and_temperature(display: Display35, data):
    forcast_area = display.get_area(0, 158, 480, 30)
    forcast_area.fill(COLOR_BACKGROUND)
    forcast_area.hline(0, 0, 480, COLOR_LINES)
    for x in range(1, 9):
        forcast_area.vline(2 + x * 53 - 1, 0, 30, COLOR_LINES)
    for x in range(1, 10):
        hour_time = clock.get_time_timestamp(data["hourly"][x]["dt"])
        hour_temp = data["hourly"][x]["temp"]
        forcast_area.text("{hour:02d}:{minute:02d}".format(hour=hour_time.hour, minute=hour_time.minute), 2 + (x - 1) * 53 + 6, 6, COLOR_TIME)
        forcast_area.text("{temp:.1f}C".format(temp=round(hour_temp, 1)).replace(".", ","), 2 + (x - 1) * 53 + 6, 22, COLOR_TEMP)
        del hour_time
        del hour_temp
        gc.collect()
    forcast_area.show()
    del forcast_area
    gc.collect()


def weather_symbols(display: Display35, data):
    print(helper.free())
    symbols_area = display.get_area(0, 158 + 30, 480, 50)
    symbols_area.fill(COLOR_BACKGROUND)
    print(helper.free())
    print("loop 1 start")
    for x in range(1, 9):
        symbols_area.vline(2 + x * 53 - 1, 0, 50, COLOR_LINES)
    print("loop 1 end")
    symbols = {}
    print(helper.free())
    print("loop 2 start")
    for x in range(1, 10):
        symbol = data["hourly"][x]["weather"][0]["icon"]
        symbols.setdefault(symbol, [])
        symbols[symbol].append(2 + (x - 1) * 53 + 1)
    print("loop 2 end")
    print(symbols)
    print(helper.free())
    print("loop 3 start")
    for symbol in symbols:
        print(helper.free())
        import images.weather_images
        icon = images.weather_images.get_image(symbol)
        for position in symbols[symbol]:
            symbols_area.add_buffer(icon, position, 0, 50)
        del icon
        del sys.modules["images.weather_images"]
        del sys.modules["images"]
        gc.collect()
    print("loop 3 end")
    print(helper.free())
    symbols_area.show()
    del symbols
    del symbols_area
    gc.collect()


def wind(display: Display35, data):
    wind_area = display.get_area(0, 158 + 30 + 50, 480, 16)
    wind_area.fill(COLOR_BACKGROUND)
    for x in range(1, 9):
        wind_area.vline(2 + x * 53 - 1, 0, 16, COLOR_LINES)
    for x in range(1, 10):
        wind_text = "{wind}km/h".format(wind=int(round(data["hourly"][x]["wind_speed"] * 3.6, 0)))
        wind_area.text(wind_text, 2 + (x - 1) * 53 + ((52 - (len(wind_text) * 8)) // 2), 2, COLOR_WIND)

    wind_area.show()
    del wind_area
    gc.collect()


def rain(display: Display35, data):
    rain_area = display.get_area(0, 158 + 30 + 50 + 16, 480, 50)
    rain_area.fill(COLOR_BACKGROUND)
    for x in range(1, 9):
        rain_area.vline(2 + x * 53 - 1, 0, 50, COLOR_LINES)
    for x in range(1, 10):
        rain_amount = 0
        if "rain" in data["hourly"][x]:
            rain_amount = math.ceil(data["hourly"][x]["rain"]["1h"])
        if rain_amount > 50:
            rain_amount = 50
        y = 50 - rain_amount
        rain_area.fill_rect(2 + (x - 1) * 53, y, 52, 50 - y, COLOR_RAIN_AMOUNT)

        percent = int(data["hourly"][x]["pop"] * 100)
        text = "{percent}%".format(percent=percent)
        rain_area.text(text, 2 + (x - 1) * 53 + ((52 - (len(text) * 8)) // 2), 4, COLOR_RAIN)
    rain_area.show()
    del rain_area
    gc.collect()
