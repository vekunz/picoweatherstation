import gc
import sys


def get_image(img: str):
    image_bytes = bytearray(0)

    if img == '01d':
        import images.weather_img_01d
        image_bytes = images.weather_img_01d.img
        del sys.modules["images.weather_img_01d"]
    if img == '01n':
        import images.weather_img_01n
        image_bytes = images.weather_img_01n.img
        del sys.modules["images.weather_img_01n"]
    if img == '02d':
        import images.weather_img_02d
        image_bytes = images.weather_img_02d.img
        del sys.modules["images.weather_img_02d"]
    if img == '02n':
        import images.weather_img_02n
        image_bytes = images.weather_img_02n.img
        del sys.modules["images.weather_img_02n"]
    if img == '03d' or img == '03n':
        import images.weather_img_03
        image_bytes = images.weather_img_03.img
        del sys.modules["images.weather_img_03"]
    if img == '04d' or img == '04n':
        import images.weather_img_04
        image_bytes = images.weather_img_04.img
        del sys.modules["images.weather_img_04"]
    if img == '09d' or img == '09n':
        import images.weather_img_09
        image_bytes = images.weather_img_09.img
        del sys.modules["images.weather_img_09"]
    if img == '10d':
        import images.weather_img_10d
        image_bytes = images.weather_img_10d.img
        del sys.modules["images.weather_img_10d"]
    if img == '10n':
        import images.weather_img_10n
        image_bytes = images.weather_img_10n.img
        del sys.modules["images.weather_img_10n"]
    if img == '11d' or img == '11n':
        import images.weather_img_11
        image_bytes = images.weather_img_11.img
        del sys.modules["images.weather_img_11"]
    if img == '13d' or img == '13n':
        import images.weather_img_13
        image_bytes = images.weather_img_13.img
        del sys.modules["images.weather_img_13"]
    if img == '50d' or img == '50n':
        import images.weather_img_50
        image_bytes = images.weather_img_50.img
        del sys.modules["images.weather_img_50"]
    gc.collect()
    return image_bytes
