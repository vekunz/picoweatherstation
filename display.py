from machine import Pin, SPI, PWM
import framebuf
import time
import gc


class Display35:
    def __init__(
            self,
            lcd_dc: int = 8,
            lcd_cs: int = 9,
            spi_sck: int = 10,
            spi_mosi: int = 11,
            spi_miso: int = 12,
            lcd_bl: int = 13,
            lcd_rst: int = 15,
            tp_cs: int = 16,
            tp_irq: int = 17
    ):
        self.width = 480
        self.height = 320

        self.spi_sck = spi_sck
        self.spi_mosi = spi_mosi
        self.spi_miso = spi_miso
        self.lcd_bl = lcd_bl

        self.lcd_cs = Pin(lcd_cs, Pin.OUT)
        self.lcd_rst = Pin(lcd_rst, Pin.OUT)
        self.lcd_dc = Pin(lcd_dc, Pin.OUT)

        self.tp_cs = Pin(tp_cs, Pin.OUT)
        self.tp_irq = Pin(tp_irq, Pin.IN)

        self.lcd_cs(1)
        self.lcd_dc(1)
        self.lcd_rst(1)
        self.tp_cs(1)
        self.spi = SPI(1, 60_000_000, sck=Pin(self.spi_sck), mosi=Pin(self.spi_mosi), miso=Pin(self.spi_miso))

        self.init_display()

    def init_display(self):
        """Initialize display"""
        self.lcd_rst(1)
        time.sleep_ms(5)
        self.lcd_rst(0)
        time.sleep_ms(10)
        self.lcd_rst(1)
        time.sleep_ms(5)
        self.write_cmd(0x21)
        self.write_cmd(0xC2)
        self.write_data(0x33)
        self.write_cmd(0XC5)
        self.write_data(0x00)
        self.write_data(0x1e)
        self.write_data(0x80)
        self.write_cmd(0xB1)
        self.write_data(0xB0)
        self.write_cmd(0x36)
        self.write_data(0x28)
        self.write_cmd(0XE0)
        self.write_data(0x00)
        self.write_data(0x13)
        self.write_data(0x18)
        self.write_data(0x04)
        self.write_data(0x0F)
        self.write_data(0x06)
        self.write_data(0x3a)
        self.write_data(0x56)
        self.write_data(0x4d)
        self.write_data(0x03)
        self.write_data(0x0a)
        self.write_data(0x06)
        self.write_data(0x30)
        self.write_data(0x3e)
        self.write_data(0x0f)
        self.write_cmd(0XE1)
        self.write_data(0x00)
        self.write_data(0x13)
        self.write_data(0x18)
        self.write_data(0x01)
        self.write_data(0x11)
        self.write_data(0x06)
        self.write_data(0x38)
        self.write_data(0x34)
        self.write_data(0x4d)
        self.write_data(0x06)
        self.write_data(0x0d)
        self.write_data(0x0b)
        self.write_data(0x31)
        self.write_data(0x37)
        self.write_data(0x0f)
        self.write_cmd(0X3A)
        self.write_data(0x55)
        self.write_cmd(0x11)
        time.sleep_ms(120)
        self.write_cmd(0x29)

        self.write_cmd(0xB6)
        self.write_data(0x00)
        self.write_data(0x62)

        self.write_cmd(0x36)
        self.write_data(0x28)

    def write_cmd(self, cmd):
        self.lcd_cs(1)
        self.lcd_dc(0)
        self.lcd_cs(0)
        self.spi.write(bytearray([cmd]))
        self.lcd_cs(1)

    def write_data(self, data):
        self.lcd_cs(1)
        self.lcd_dc(1)
        self.lcd_cs(0)
        self.spi.write(bytearray([data]))
        self.lcd_cs(1)

    def write_buffer(self, buf):
        self.lcd_cs(1)
        self.lcd_dc(1)
        self.lcd_cs(0)
        self.spi.write(buf)
        self.lcd_cs(1)

    def get_area(self, x, y, width, height):
        return DrawArea(display=self, x=x, y=y, width=width, height=height)

    def backlight(self, percent):
        pwm = PWM(Pin(self.lcd_bl))
        pwm.freq(1000)
        if percent >= 100:
            pwm.duty_u16(65535)
        else:
            pwm.duty_u16(655*percent)

    def touch_get(self):
        if self.tp_irq() == 0:
            self.spi = SPI(1, 5_000_000, sck=Pin(self.spi_sck), mosi=Pin(self.spi_mosi), miso=Pin(self.spi_miso))
            self.tp_cs(0)
            x_point = 0
            y_point = 0
            for i in range(0, 3):
                self.spi.write(bytearray([0xD0]))
                read_date = self.spi.read(2)
                time.sleep_us(10)
                y_point = y_point + (((read_date[0] << 8) + read_date[1]) >> 3)

                self.spi.write(bytearray([0x90]))
                read_date = self.spi.read(2)
                x_point = x_point + (((read_date[0] << 8) + read_date[1]) >> 3)

            x_point = x_point / 3
            y_point = y_point / 3

            x_point = int((x_point - 430) * 480 / 3270)
            y_point = 320 - int((y_point - 430) * 320 / 3270)

            if x_point < 0:
                x_point = 0
            elif x_point > 480:
                x_point = 480
            if y_point < 0:
                y_point = 0
            elif y_point > 320:
                y_point = 320

            self.tp_cs(1)
            self.spi = SPI(1, 60_000_000, sck=Pin(self.spi_sck), mosi=Pin(self.spi_mosi), miso=Pin(self.spi_miso))
            return [x_point, y_point]


class DrawArea(framebuf.FrameBuffer):
    def __init__(
            self,
            display: Display35,
            x: int,
            y: int,
            width: int,
            height: int,
    ):
        self.display = display
        self.x = x
        self.y = y
        self.width = width
        self.height = height

        self.buffer = bytearray(height * width * 2)
        super().__init__(self.buffer, self.width, self.height, framebuf.RGB565)

    def set_buffer(self, buffer):
        self.buffer = buffer

    def add_buffer(self, buffer, x_cord, y_cord, width):
        height = (len(buffer) // 2) // width

        skip_bytes_base = y_cord * self.width * 2

        for y in range(0, height):
            skip_bytes = skip_bytes_base + (y * self.width * 2)
            for x in range(0, width):
                position_new = skip_bytes + ((x_cord + x) * 2)
                position_old = (y * width * 2) + (x * 2)
                self.buffer[position_new] = buffer[position_old]
                self.buffer[position_new + 1] = buffer[position_old + 1]
                del position_old
                del position_new
            del skip_bytes
        del height
        del skip_bytes_base
        gc.collect()

    def show(self):
        x_left_byte = self.x >> 8
        x_right_byte = self.x & 0xFF
        y_left_byte = self.y >> 8
        y_right_byte = self.y & 0xFF
        width_left_byte = (self.x + self.width - 1) >> 8
        width_right_byte = (self.x + self.width - 1) & 0xFF
        height_left_byte = (self.y + self.height - 1) >> 8
        height_right_byte = (self.y + self.height - 1) & 0xFF

        self.display.write_cmd(0x2A)  # Column Address Set
        self.display.write_data(x_left_byte)
        self.display.write_data(x_right_byte)
        self.display.write_data(width_left_byte)
        self.display.write_data(width_right_byte)

        self.display.write_cmd(0x2B)  # Page Address Set
        self.display.write_data(y_left_byte)
        self.display.write_data(y_right_byte)
        self.display.write_data(height_left_byte)
        self.display.write_data(height_right_byte)

        self.display.write_cmd(0x2C)  # 44

        self.display.write_buffer(self.buffer)
