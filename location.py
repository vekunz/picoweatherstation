import urequests
import gc


class Location:
    lon: float
    lat: float
    name: str
    country: str
    state: str

    def __init__(self, lon: float, lat: float, name: str, country: str, state: str):
        self.lon = lon
        self.lat = lat
        self.name = name
        self.country = country
        self.state = state


def get_location_data(lon: float, lat: float, appid: str):
    r = urequests.get("https://api.openweathermap.org/geo/1.0/reverse?lat={lat}&lon={lon}&appid={appid}".format(lat=lat, lon=lon, appid=appid))
    if r.status_code >= 500:
        raise Exception("{code} {reason}".format(code=r.status_code, reason=r.reason))
    elif r.status_code >= 400:
        data = r.json()
        raise Exception("{code} {reason} - {message}".format(code=r.status_code, reason=r.reason, message=data["message"]))

    data = r.json()
    loc = Location(lon=data[0]["lon"], lat=data[0]["lat"], name=data[0]["name"], country=data[0]["country"], state=data[0]["state"])
    del r
    del data
    gc.collect()
    return loc
