from machine import RTC
import time
import gc
import urequests
import machine

utc_offset = 0


class Time:
    year: int
    month: int
    day: int
    hour: int
    minute: int
    second: int
    weekday: int

    def __init__(self, year: int, month: int, day: int, hour: int, minute: int, second: int, weekday: int):
        self.year = year
        self.month = month
        self.day = day
        self.hour = hour
        self.minute = minute
        self.second = second
        self.weekday = weekday


def init_clock(timezone: str):
    global utc_offset
    try:
        r = urequests.get("https://worldtimeapi.org/api/timezone/" + timezone)
        data = r.json()

        utc_offset = data["raw_offset"]
        if data["dst"]:
            utc_offset = utc_offset + data["dst_offset"]

        unix_time = data["unixtime"] + utc_offset
        lt = time.gmtime(unix_time)

        del r
        del data
        gc.collect()

        rtc = machine.RTC()
        rtc.datetime((lt[0], lt[1], lt[2], 0, lt[3], lt[4], lt[5], 0))
        return True
    except:
        return False


def get_time():
    rtc = RTC()
    now = rtc.datetime()
    return Time(year=now[0], month=now[1], day=now[2], hour=now[4], minute=now[5], second=now[6], weekday=now[3])


def get_time_timestamp(timestamp: int):
    lt = time.gmtime(timestamp + utc_offset)
    return Time(year=lt[0], month=lt[1], day=lt[2], hour=lt[3], minute=lt[4], second=lt[5], weekday=lt[6])


def weekday_name(weekday: int):
    weekdays = ["Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"]

    if weekday < 0:
        weekday = 0
    elif weekday > 6:
        weekday = 6

    return weekdays[weekday]
